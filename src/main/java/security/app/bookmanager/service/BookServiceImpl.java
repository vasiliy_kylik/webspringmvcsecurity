package security.app.bookmanager.service;

import security.app.bookmanager.dao.BookDao;
import security.app.bookmanager.dao.BookDaoImpl;
import security.app.bookmanager.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Молния on 25.02.2017.
 */
@Service
public class BookServiceImpl implements BookService {

    @Qualifier("bookDaoImpl")
    @Autowired

    private BookDao bookDao;

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    @Transactional
    public void addBook(Book book) {
        this.bookDao.addBook(book);
    }

    @Override
    @Transactional
    public void updateBook(Book book) {
        this.bookDao.updateBook(book);
    }

    @Override
    @Transactional
    public void removeBook(Long id) {
        this.bookDao.removeBook(id);
    }

    @Override
    @Transactional
    public Book getBookById(Long id) {
        return this.bookDao.getBookById(id);
    }

    @Override
    @Transactional
    public List<Book> listBooks() {
        return this.bookDao.listBooks();
    }

    @Override
    public Book getBookByTitle(String title) {
        return this.bookDao.getBookByTitle(title);
    }
}
