package security.app.bookmanager.service;

import security.app.bookmanager.model.User;

/**
 * Created by Молния on 26.02.2017.
 */
public interface UserService {
    void save(User user);
    User findByUsername(String username);
}
