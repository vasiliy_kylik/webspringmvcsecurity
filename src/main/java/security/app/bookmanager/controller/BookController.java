package security.app.bookmanager.controller;

import security.app.bookmanager.model.Book;
import security.app.bookmanager.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Молния on 25.02.2017.
 */
@Controller
public class BookController {
  @Autowired
  private BookService bookService;

/*
    @Autowired(required = true)
    @Qualifier(value = "bookService")
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }
*/

  @RequestMapping(value = "books", method = RequestMethod.GET)
  public String listBooks(Model model) {
    model.addAttribute("book", new Book());
    model.addAttribute("listBooks", this.bookService.listBooks());

    return "books";
  }

  @RequestMapping(value = "/books/add", method = RequestMethod.POST)
  public String addBook(@ModelAttribute("book") Book book, Model model) {
    try {
      book.getId();
    } catch (NullPointerException e) {
      this.bookService.addBook(book);
    }
    this.bookService.updateBook(book);
    model.addAttribute("book", this.bookService.listBooks());
    return "redirect:/books";
  }

  @RequestMapping("/remove/{id}")
  public String removeBook(@PathVariable("id") Long id) {
    this.bookService.removeBook(id);
    return "redirect:/books";
  }

  @RequestMapping("edit/{id}")
  public String editBook(@PathVariable("id") Long id, Model model) {
    model.addAttribute("book", this.bookService.getBookById(id));
    model.addAttribute("listBooks", this.bookService.listBooks());
    return "books";
  }

  @RequestMapping("bookdata/{id}")
  public String bookData(@PathVariable("id") Long id, Model model) {
    model.addAttribute("book", this.bookService.getBookById(id));
    return "bookdata";
  }

  @RequestMapping("bookdataTitle/{bookTitle}")
  public String bookDataTitle(@PathVariable("bookTitle") String title, Model model) {
    model.addAttribute("book", this.bookService.getBookByTitle(title));
    return "books";
  }
}

