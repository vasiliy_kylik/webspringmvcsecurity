package security.app.bookmanager.model;


import javax.persistence.*;

/**
 * Created by Молния on 25.02.2017.
 */
@Entity
@Table(name = "books")
public class Book {
    @Id
    @Column(name = "ID")@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "BOOK_TITLE")
    private String bookTitle;
    @Column(name= "BOOK_AUTHOR")
    private String bookAuthor;
    @Column(name= "BOOK_PRICE")
    private int price;

    public Book() {
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookTitle='" + bookTitle + '\'' +
                ", bookAuthor='" + bookAuthor + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (price != book.price) return false;
        if (id != null ? !id.equals(book.id) : book.id != null) return false;
        if (bookTitle != null ? !bookTitle.equals(book.bookTitle) : book.bookTitle != null) return false;
        return bookAuthor != null ? bookAuthor.equals(book.bookAuthor) : book.bookAuthor == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (bookTitle != null ? bookTitle.hashCode() : 0);
        result = 31 * result + (bookAuthor != null ? bookAuthor.hashCode() : 0);
        result = 31 * result + price;
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
