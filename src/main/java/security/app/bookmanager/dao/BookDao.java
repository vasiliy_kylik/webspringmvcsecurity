package security.app.bookmanager.dao;

import security.app.bookmanager.model.Book;

import java.util.List;

/**
 * Created by Молния on 25.02.2017.
 */
public interface BookDao  {
    public void addBook(Book book);
    public void updateBook(Book book);
    public void removeBook(Long id);
    public Book getBookById(Long id);
    public List<Book> listBooks();
    Book getBookByTitle(String title);
}
