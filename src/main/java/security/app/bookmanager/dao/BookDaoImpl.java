package security.app.bookmanager.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import security.app.bookmanager.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Молния on 25.02.2017.
 */
@Repository

public class BookDaoImpl implements BookDao {
  private static final Logger logger = LoggerFactory.getLogger(BookDaoImpl.class);


  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public void addBook(Book book) {
    this.entityManager.persist(book);
    logger.info("Book successfully persist(addBook). Book details: " + book);
  }

  @Override
  public void updateBook(Book book) {
/*    if (getBookById(book.getId())!=null) {

    }*/
    this.entityManager.merge(book);
    logger.info("Book successfully merge(updateBook). Book details: " + book);
  }

  @Override
  public void removeBook(Long id) {
    Book book = getBookById(id);
    if (book != null) {
      this.entityManager.remove(book);
    }
    logger.info("Book successfully removed. Book details: " + book);
  }

  @Override
  public Book getBookById(Long id) {
    Query query = this.entityManager.createQuery(
            "SELECT DISTINCT book FROM Book book WHERE book.id=:id"
    );
    query.setParameter("id", id);
    Book book = (Book) query.getSingleResult();
    logger.info("Book successfully loadded. Book details: " + book);
    return book;
  }

  @Override
  public List<Book> listBooks() {
    List<Book> result;

    Query query = this.entityManager.createQuery(
            "SELECT DISTINCT book FROM Book book");
    result = query.getResultList();
    for (Book book : result) {
      logger.info("Book list: " + book);
    }
    return result;
  }


  public Book getBookByTitle(String title) {
    Query query = this.entityManager.createQuery(
            "select distinct book from Book book where book.bookTitle=:title"
    );
    query.setParameter("title", title);
    Book book = (Book) query.getSingleResult();
    return book;
  }
/*  @Override
  public List<Book> getBookByTitle(String title) {
    List<Book> result;
    Query query = this.entityManager.createQuery(
            "select distinct book from Book book where book.bookTitle=:title"
    );
    query.setParameter("title", title);
    result = query.getResultList();
    for (Book book : result) {
      logger.info("Book list by Title: " + book);
    }
    return result;
  }*/
}
