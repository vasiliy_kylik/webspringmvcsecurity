/*
package security.app.bookmanager.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import security.app.bookmanager.dao.BookDao;
import security.app.bookmanager.dao.BookDaoImpl;
import security.app.bookmanager.model.Book;

import java.util.List;

*/
/**
 * Created by Vasiliy Kylik on 30.05.2017.
 *//*

@ContextConfiguration(locations = "classpath:appconfig-data-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class TestBookDAO {
    @Autowired
    private BookDaoImpl bookDao;

    @Test
    @Transactional
    @Rollback(true)
    public void testAddBook() {
        Book book = new Book();
        bookDao.addBook(book);
        List<Book> books = bookDao.listBooks();
        Assert.assertEquals(book.getBookTitle(), books.get(0).getBookTitle());
    }

}
*/
